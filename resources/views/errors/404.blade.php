@extends('layout')

@section('title', "Página no encontrada")

@section('content')
    <h1 class="text-center">Página no encontrada</h1>
    <p class="text-center"><img src="{{ asset('img/404.png') }}" alt="Error 404 Pic"></p>
@endsection

@section('sidebar')
@endsection