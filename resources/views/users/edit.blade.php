@extends('layout')

@section('title', "{$title}")

@section('content')
    <h1>Editar usuario</h1>

    @if($errors->any())
        <div class="alert alert-danger">
            <h6>Por favor, corrige los siguientes errores:</h6>
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ url("usuarios/{$user->id}") }}">
        {{ method_field('PUT') }}
        {{ csrf_field() }}

        <div class="form-group">
            <label for="name">Nombre:</label>
            {{--En este caso old() intentará cargar los datos referentes al campo name
            que se encuentren en la sesión y si no los consigue cargará el valor de
            $user->name.--}}
            <input type="text" class="form-control" name="name" id="name" placeholder="Pepito Romero" value="{{ old('name', $user->name) }}">
        </div>
        <div class="form-group">
            <label for="email">Correo electrónico:</label>
            <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="pepito@example.com" value="{{ old('email', $user->email) }}">
            <small id="emailHelp" class="form-text text-muted">Nosotros jamás compartiremos tu correo electrónico con ninguna persona.</small>
        </div>

        <div class="form-group">
            <label for="password">Contraseña:</label>
            <input type="password" class="form-control" name="password" id="password" placeholder="Mayor a 6 caracteres">
        </div>

        <button type="submit" class="btn btn-primary">Actualizar usuario</button>

    </form>

    <p>
        <a href="{{ route("users.index") }}">Regresar al listado de usuarios</a>
    </p>
@endsection

@section('sidebar')
@endsection