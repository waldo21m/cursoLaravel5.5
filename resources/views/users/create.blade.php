@extends('layout')

@section('title', "{$title}")

@section('content')
    <div class="card">
        <h4 class="card-header">Crear usuario</h4>
        <div class="card-body">
            @if($errors->any())
                <div class="alert alert-danger">
                    <h6>Por favor, corrige los errores debajo:</h6>

                    {{--<h6>Por favor, corrige los siguientes errores:</h6>--}}
                    {{--<ul>--}}
                    {{--@foreach($errors->all() as $error)--}}
                    {{--<li>{{ $error }}</li>--}}
                    {{--@endforeach--}}
                    {{--</ul>--}}
                </div>
            @endif

            <form method="POST" action="{{ url('usuarios') }}">
                {{--El middleware VerifyCsrfToken nos permite evitar que terceros puedan enviar
                 peticiones de tipo POST a nuestra aplicación y realizar ataques de tipo cross-site
                 request forgery. Para agregar un campo con el token dentro de nuestro formulario,
                 que le va a permitir a Laravel reconocer peticiones de formulario que sean
                 válidas, debemos llamar al método csrf_field()--}}
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name">Nombre:</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Pepito Romero" value="{{ old('name') }}">
                    @if($errors->has('name'))
                        <p class="text-danger">{{ $errors->first('name') }}</p>
                    @endif
                </div>
                <div class="form-group">
                    <label for="email">Correo electrónico:</label>
                    <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="pepito@example.com" value="{{ old('email') }}">
                    <small id="emailHelp" class="form-text text-muted">Nosotros jamás compartiremos tu correo electrónico con ninguna persona.</small>
                    @if($errors->has('email'))
                        <p class="text-danger">{{ $errors->first('email') }}</p>
                    @endif
                </div>

                <div class="form-group">
                    <label for="password">Contraseña:</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Mayor a 6 caracteres">
                    @if($errors->has('password'))
                        <p class="text-danger">{{ $errors->first('password') }}</p>
                    @endif
                </div>

                <button type="submit" class="btn btn-primary">Crear usuario</button>
                <a href="{{ route("users.index") }}" class="btn btn-link">Regresar al listado de usuarios</a>
            </form>
        </div>
    </div>
@endsection

@section('sidebar')
@endsection