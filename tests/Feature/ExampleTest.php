<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        // Esta ruta debe coincidir con la ruta de web.php
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
