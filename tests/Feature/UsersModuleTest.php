<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersModuleTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function it_shows_the_users_list()
    {
        //Corregir las pruebas automatizadas para un listado dinámico
        factory(User::class)->create([
            'name' => 'Eduardo',
        ]);

        factory(User::class)->create([
            'name' => 'Ellie',
        ]);

        // $this->assertTrue(true);
        $this->get('usuarios')
        	->assertStatus(200)
        	->assertSee('Listado de usuarios')
            ->assertSee('Eduardo')
            ->assertSee('Ellie');
    }

    /** @test */
    function it_shows_a_default_message_if_the_users_list_is_empty()
    {
        /*
         * Las pruebas automatizadas para un listado dinámico deben realizarse en otra BDD. Así, no
         * afectará nuestra interacción con la aplicación en local y viceversa
         */

        /*
         * La siguiente línea ya no es necesario porque, RefreshDatabase se encarga de crear los dos
         * registros necesarios para la prueba 'it_shows_the_users_list' con el factory. Pero al
         * terminar dicha prueba, se revierten los registros (porque realiza un rollback). Por lo
         * tanto, no habrán registros en la tabla users
         */
        #DB::table('users')->truncate();

        // $this->assertTrue(true);
        $this->get('usuarios')
            ->assertStatus(200)
            ->assertSee('Listado de usuarios')
            ->assertSee('No hay usuarios registrados.');
    }

    /** @test */
    function it_displays_the_users_details()
    {
        //Prueba de forma dinámica
        $user = factory(User::class)->create([
           'name' => 'Eduardo Márquez'
        ]);

        // $this->assertTrue(true);
        $this->get('usuarios/'.$user->id)
        	->assertStatus(200)
        	->assertSee(
        	    'Eduardo Márquez');
    }

    /** @test */
    function it_displays_a_404_error_if_the_user_is_not_found()
    {
        $this->get('/usuarios/999')
            ->assertStatus(404)
            ->assertSee('Página no encontrada');
    }

    /** @test */
    function it_loads_the_new_users_page()
    {
        // $this->assertTrue(true);

        // Con este comando, podemos observar en la terminal los errores de Laravel.log (en caso de que existan)
        $this->withoutExceptionHandling();

        $this->get('usuarios/nuevo')
        	->assertStatus(200)
        	->assertSee('Crear usuario');
    }

    /** @test */
    function it_creates_a_new_user()
    {
        //Comprobamos que está pasando
        $this->withoutExceptionHandling();

        $this->post('/usuarios/', [
            "name" => "Eduardo",
            "email" => "eduardo@mail.com",
            "password" => "laravel",
        ])->assertRedirect("usuarios");
        //en caso de no usar la url se coloca ->assertRedirect(route('users.index'));

        //Comprobar usuario registrado en la BDD con assertDatabaseHas
        $this->assertDatabaseHas('users', [
            "name" => "Eduardo",
            "email" => "eduardo@mail.com",
            //"password" => "laravel",
        ]);

        //Comprobar usuario con el método assertCredentials
        $this->assertCredentials([
            "name" => "Eduardo",
            "email" => "eduardo@mail.com",
            "password" => "laravel",
        ]);

        /*
         * Se puede usar cualquiera de los dos métodos para realizar la comprobación, la diferencia radica
         * en que el método assertDatabaseHas no comprueba valores encriptados.
         */
    }

    /** @test */
    function the_name_is_required()
    {
        /*
         * Si no desactivo este manejador de excepciones, no pasarán las pruebas porque se está enviando
         * el campo name vacío (es un campo invalido).
         */
        //$this->withoutExceptionHandling();

        /*
         * El método validate del objeto request nos redirecciona a la vista anterior. Por eso, usamos el
         * método from para corregir la prueba
         */
        $this->from("usuarios/nuevo")->post('/usuarios/', [
            "name" => "",
            "email" => "eduardo@mail.com",
            "password" => "laravel",
        ])
        ->assertRedirect("usuarios/nuevo")
        //Sin ser explicito en el mensaje de error
        //->assertSessionHasErrors(['name']);
        //Siendo explicitos en el mensaje de error
        ->assertSessionHasErrors(['name' => 'El campo nombre es obligatorio']);

        /*
         * El metodo assertEquals comprueba que el usuario no está siendo guardado en la
         * BDD. A este método le pasamos el valor esperado como primer argumento y como
         * segundo argumento pasamos el número de usuarios en la base de datos,
         * utilizando el método count que provee Eloquent
         */
        $this->assertEquals(0, User::count());

        $this->assertDatabaseMissing('users', [
            'email' => 'eduardo@mail.com',
        ]);

        /*
         * Se puede usar cualquiera de los dos métodos para realizar la comprobación.
         */
    }

    /** @test */
    function the_email_is_required()
    {
        $this->from("usuarios/nuevo")->post('/usuarios/', [
            "name" => "Eduardo",
            "email" => "",
            "password" => "laravel",
        ])
            ->assertRedirect("usuarios/nuevo")
            ->assertSessionHasErrors(['email' => 'El campo email es obligatorio']);

        $this->assertEquals(0, User::count());
    }

    /** @test */
    function the_email_must_be_valid()
    {
        $this->from("usuarios/nuevo")->post('/usuarios/', [
            "name" => "Eduardo",
            "email" => "correo-no-valido",
            "password" => "laravel",
        ])
            ->assertRedirect("usuarios/nuevo")
            ->assertSessionHasErrors(['email' => 'El campo email no es valido']);

        $this->assertEquals(0, User::count());
    }

    /** @test */
    function the_email_must_be_unique()
    {
        //$this->withoutExceptionHandling();

        factory(User::class)->create([
            'email' => 'eduardo@mail.com',
        ]);

        $this->from("usuarios/nuevo")->post('/usuarios/', [
            "name" => "Eduardo",
            "email" => "eduardo@mail.com",
            "password" => "laravel",
        ])
            ->assertRedirect("usuarios/nuevo")
            ->assertSessionHasErrors(['email' => 'Este email ya está registrado']);

        $this->assertEquals(1, User::count());
    }

    /** @test */
    function the_password_is_required()
    {
        $this->from("usuarios/nuevo")->post('/usuarios/', [
            "name" => "Eduardo",
            "email" => "eduardo@mail.com",
            "password" => "",
        ])
            ->assertRedirect("usuarios/nuevo")
            ->assertSessionHasErrors(['password' => 'El campo password es obligatorio']);

        $this->assertEquals(0, User::count());
    }

    /** @test */
    function the_password_must_be_greater_than_six_characters()
    {
        $this->from("usuarios/nuevo")->post('/usuarios/', [
            "name" => "Eduardo",
            "email" => "eduardo@mail.com",
            "password" => "12345",
        ])
            ->assertRedirect("usuarios/nuevo")
            ->assertSessionHasErrors(['password' => 'El campo password debe ser mayor a 6 caracteres']);

        $this->assertEquals(0, User::count());
    }

    /** @test */
    function it_loads_the_edit_user_page()
    {
        $this->withoutExceptionHandling();

        $user = factory(User::class)->create();

        $this->get("usuarios/{$user->id}/editar")
            ->assertStatus(200)
            ->assertViewIs('users.edit')
            ->assertSee('Editar usuario')
            ->assertViewHas('user', function ($viewUser) use ($user) {
                return $viewUser->id === $user->id;
            }); //Verifica si hay una variable user en la vista que coincida con el objeto que crea factory
    }

    /** @test */
    function it_updates_a_user()
    {
        $user = factory(User::class)->create();

        $this->withoutExceptionHandling();

        $this->put("/usuarios/{$user->id}", [
            "name" => "Eduardo",
            "email" => "eduardo@mail.com",
            "password" => "laravel",
        ])->assertRedirect("/usuarios/{$user->id}"); // (users.show)

        $this->assertCredentials([
            "name" => "Eduardo",
            "email" => "eduardo@mail.com",
            "password" => "laravel",
        ]);
    }

    /** @test */
    function the_name_is_required_when_updating_a_user()
    {
        $user = factory(User::class)->create();

        $this->from("/usuarios/{$user->id}/editar")->put("/usuarios/{$user->id}", [
            "name" => "",
            "email" => "eduardo@mail.com",
            "password" => "laravel",
        ])
            ->assertRedirect("/usuarios/{$user->id}/editar")
            ->assertSessionHasErrors(['name' => 'El campo nombre es obligatorio']);

        $this->assertDatabaseMissing('users', [
            'email' => 'eduardo@mail.com',
        ]);
    }

    /** @test */
    function the_email_is_required_when_updating_a_user()
    {
        $user = factory(User::class)->create();

        $this->from("/usuarios/{$user->id}/editar")->put("/usuarios/{$user->id}", [
            "name" => "Eduardo",
            "email" => "",
            "password" => "laravel",
        ])
            ->assertRedirect("/usuarios/{$user->id}/editar")
            ->assertSessionHasErrors(['email' => 'El campo email es obligatorio']);

        $this->assertDatabaseMissing('users', [
            'name' => 'Eduardo',
        ]);
    }

    /** @test */
    function the_email_must_be_valid_when_updating_a_user()
    {
        $user = factory(User::class)->create(['name' => 'Nombre viejo']);

        $this->from("/usuarios/{$user->id}/editar")->put("/usuarios/{$user->id}", [
            "name" => "Nombre actualizado",
            "email" => "correo-no-valido",
            "password" => "laravel",
        ])
            ->assertRedirect("/usuarios/{$user->id}/editar")
            ->assertSessionHasErrors(['email' => 'El campo email no es valido']);

        $this->assertDatabaseMissing('users', [
            'name' => 'Eduardo',
        ]);
    }

    /** @test */
    function the_users_email_can_stay_the_same_when_updating_the_user()
    {
        $user = factory(User::class)->create([
            'email' => 'eduardo@mail.com',
        ]);

        $this->from("/usuarios/{$user->id}/editar")->put("/usuarios/{$user->id}", [
            "name" => "Eduardo",
            "email" => "eduardo@mail.com",
            "password" => "laravel",
        ])
            ->assertRedirect("/usuarios/{$user->id}");

        $this->assertDatabaseHas('users', [
            "name" => "Eduardo",
            "email" => "eduardo@mail.com",
        ]);
    }

    /** @test */
    function the_email_must_be_unique_when_updating_a_user()
    {
        factory(User::class)->create([
            'email' => 'exisiting-email@example.com',
        ]);

        $user = factory(User::class)->create([
            'email' => 'eduardo@mail.com',
        ]);

        $this->from("/usuarios/{$user->id}/editar")->put("/usuarios/{$user->id}", [
            "name" => "Eduardo",
            "email" => "exisiting-email@example.com",
            "password" => "laravel",
        ])
            ->assertRedirect("/usuarios/{$user->id}/editar")
            ->assertSessionHasErrors(['email']);

        //
    }

    /** @test */
    function the_password_is_optional_when_updating_a_user()
    {
        $oldPassword = 'CLAVE_ANTERIOR';

        $user = factory(User::class)->create([
            'password' => bcrypt($oldPassword),
        ]);

        $this->from("/usuarios/{$user->id}/editar")->put("/usuarios/{$user->id}", [
            "name" => "Eduardo",
            "email" => "eduardo@mail.com",
            "password" => "",
        ])
            ->assertRedirect("/usuarios/{$user->id}"); // (users.show)

        $this->assertCredentials([
            "name" => "Eduardo",
            "email" => "eduardo@mail.com",
            "password" => $oldPassword,
        ]);
    }

    /** @test */
    function the_password_must_be_greater_than_six_characters_when_updating_a_user()
    {
        $user = factory(User::class)->create();

        $this->from("/usuarios/{$user->id}/editar")->put("/usuarios/{$user->id}", [
            "name" => "Eduardo",
            "email" => "eduardo@mail.com",
            "password" => "12345",
        ])
            ->assertRedirect("/usuarios/{$user->id}/editar")
            ->assertSessionHasErrors(['password' => 'El campo password debe ser mayor a 6 caracteres']);

        $this->assertDatabaseMissing('users', [
            'email' => 'eduardo@mail.com',
        ]);
    }

    /** @test */
    function it_deletes_a_user()
    {
        $user = factory(User::class)->create([
            'email' => 'eduardo@mail.com'
        ]);

        $this->delete("/usuarios/{$user->id}")
            ->assertRedirect("usuarios");

        $this->assertDatabaseMissing('users', [
            'id' => $user->id,
        ]);

        $this->assertSame(0, User::count());

        /*
         * Cualquiera de los dos métodos es válido para verificar
         */
    }
}
