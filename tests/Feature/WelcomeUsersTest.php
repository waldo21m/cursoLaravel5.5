<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WelcomeUsersTest extends TestCase
{
	/** @test */
    function it_welcomes_users_name()
    {
        $this->get('/saludo/nombre/eduardo')
        	->assertStatus(200)
        	->assertSee('Bienvenido Eduardo!');
    }

    /** @test */
    function it_welcomes_users_with_nickname()
    {
        $this->get('/saludo/apodo/waldo')
        	->assertStatus(200)
        	->assertSee('Bienvenido waldo!');
    }

    /** @test */
    function it_welcomes_users_without_nickname()
    {
        $this->get('/saludo/apodo')
            ->assertStatus(200)
            ->assertSee('Bienvenido anonimo!');
    }
}
