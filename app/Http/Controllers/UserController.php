<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    // Si cometemos un error, en el directorio /storage/logs/ podemos observar los errores
    public function index()
    {
        //Usando el manejador de consultas de Laravel
        #$users = DB::table('users')->get();

        //Usando ORM Eloquent
        $users = User::all();

        /*
         * Al usar Eloquent, si imprimimos el objeto 'Users' no ocurrirá un error porque cada objeto (o cada)
         * usuario va a representar una instancia de la clase User. En cambio, con el constructor de consultas
         * de Laravel tendremos diversos objetos que no son instancias de un modelo de Eloquent.
         */
        #dd($users);

        $title = 'Listado de usuarios';

        //Otra forma de pasarlo
        #return view('users.index')->with('users', User::all())->with('title', 'Listado de usuarios');

        return view('users.index', compact('title', 'users'));
    }

    /*
     * En lugar de obtener el usuario utilizando los métodos find o findOrFail podemos obtenerlo directamente
     * como parámetro de la acción.
     * Para que esto funcione, nota que el nombre del parámetro en la declaración de la ruta debe coincidir
     * con el nombre del parámetro en la declaración del método.
     * Además el tipo del parámetro debe ser, por supuesto, un modelo de Eloquent
     * (en nuestro ejemplo es App\User).
     */
    public function show(User $user)
    {
        /*
         * Laravel nos provee de un método que nos ahorra tener que escribir el código if que se ve
         * a continuación. Lo que hace es buscar por el id y en caso de no encontrarlo, Laravel
         * devolverá una excepción de tipo ModelNotFoundException cuyo objetivo es renderizar
         * con el código de status 404 a la vista que se almacene en errors.404
         */

        //$user = User::findOrFail($id);
        //También existe un método llamado firstOrFail

        /*if ($user == null) {
            //El test fallará porque en sí da un código de status 200 y no 404
            //return view('errors.404');

            //Manera correcta de como se debe pasar
            return response()->view('errors.404', [], 404);
        }*/

        $title = "Usuario";

        return view('users.show', compact('title', 'user'));
    }

    public function create()
    {
        $title = "Crear usuario";

        return view('users.create', compact('title'));
    }

    public function store()
    {
        /*
         * Enviar de forma manual los valores de los campos inputs.
         *
         * Laravel se encarga de enviar de forma automática los campos de los inputs y los mensajes
         * de errores con el método validate del objeto request, pero con el siguiente código, se
         * puede enviar de forma manual los valores de los campos
         */
        //return redirect('usuarios/nuevo')->withInput();

        /*
         * Podemos llamar al método validate() en el objeto request pasando como valor un array
         * asociativo donde cada llave será el nombre de cada campo esperado y el valor una
         * cadena con las reglas de validación
         *
         * Este método te retorna a la url anterior
         */

        //Reglas de validación en https://laravel.com/docs/5.5/validation
        $data = request()->validate([
            'name' => 'required',
            //Reglas de validación vacias
            //'email' => '',
            'email' => 'required|email|unique:users,email', //unique:tabla,campo
            //Otra forma de pasar la regla
            //'email' => ['required', 'email'],
            'password' => 'required|between:6,50',
        ], [
            'name.required' => 'El campo nombre es obligatorio', // Forma de escribir el mensaje personalizado
            'email.required' => 'El campo email es obligatorio',
            'email.email' => 'El campo email no es valido',
            'email.unique' => 'Este email ya está registrado',
            'password.required' => 'El campo password es obligatorio',
            'password.between' => 'El campo password debe ser mayor a 6 caracteres',
        ]);

        //Validaciones de forma rudimentaria
        //if (empty($data['name'])) {
        //    return redirect('usuarios/nuevo')->withErrors([
        //        'name' => 'El campo nombre es obligatorio',
        //    ]);
        //}

        User::create([
            "name" => $data["name"],
            "email" => $data["email"],
            "password" => bcrypt($data["password"]),
        ]);

        //return redirect("usuarios");
        return redirect()->route("users.index");
    }

    public function edit(User $user)
    {
        $title = "Editar usuario";

        return view('users.edit', compact('title', 'user'));
    }

    public function update(User $user)
    {
        $data = request()->validate([
            'name' => 'required',
            //'email' => 'required|email|unique:users,email,'.$user->id,
            //Otra forma de escribirlo
            'email' => ['required', 'email', Rule::unique('users', 'email')->ignore($user->id)],
            'password' => 'nullable|between:6,50',
        ], [
            'name.required' => 'El campo nombre es obligatorio', // Forma de escribir el mensaje personalizado
            'email.required' => 'El campo email es obligatorio',
            'email.email' => 'El campo email no es valido',
            'email.unique' => 'Este email ya está registrado',
            'password.between' => 'El campo password debe ser mayor a 6 caracteres',
        ]);

        if ($data['password'] != null) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        $user->update($data);

        return redirect()->route('users.show', ['user' => $user]);
    }

    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('users.index');
    }
}
