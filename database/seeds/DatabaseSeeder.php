<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Se puede usar de dos maneras, como: <<ProfessionSeeder::class>> o simplemente el
        //nombre de la clase <<'ProfessionSeeder'>>. El problema radica que cuando se
        //le indica mal nombre, Laravel no te indicará cuál es el error.
        //dd('ProfessionSeeder');
        $this->truncateTables([
            'professions',
            'users',
        ]);

        // $this->call(UsersTableSeeder::class);
        $this->call(ProfessionSeeder::class);
        $this->call(UserSeeder::class);
    }

    public function truncateTables(array $tables)
    {
        //La siguiente sentencia sirve para eliminar la revisión de llaves foráneas en la BDD
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        //Sentencia para eliminar los registros de la tabla
        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }
        //Activar revisión de llaves foráneas
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
